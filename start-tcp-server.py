# -*- coding: utf-8 -*-
import sys
import SocketServer


class MyTCPHandler(SocketServer.StreamRequestHandler):


    def handle(self):
        _handlers = {'common': self.common_cmd,
                     'search': self.search_cmd}
        data = self.rfile.readline().strip()
        l = data.split()
        if len(l) >= 2:
            command, args = l[0], l[1:]
            f = _handlers.get(command, None)
            if f:
                ans = f(*args)
            else:
                ans = 'Invalid command'
        else:
            ans = 'Invalid Usage'
        self.request.sendall(ans + '\r\n')

    def common_cmd(self, *args):
        """Add your code here for the common command
        This function should return a string with the
        most n most common words in the books
        """
        ans = []
        # TODO
        return '\n'.join(ans)

    def search_cmd(self, *args):
        """Add your code here for the search command
        Should return a a string with the documents the
        word appears into"""
        ans = []
        # TODO
        return '\n'.join(ans)


if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 9999

    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
    server.serve_forever()

